+++
title = "Authors"
description = "The authors of the blog articles."
# date = 2023-01-02T17:50:45+00:00
# updated = 2023-01-02T17:50:45+00:00
draft = false

# If add a new author page in this section, please add a new item,
# and the format is as follows:
#
# "author-name-in-url" = "the-full-path-of-the-author-page"
#
# Note: We use quoted keys here.
[extra.author_pages]
"megian" = "authors/megian.md"
+++

The authors of the blog articles.
