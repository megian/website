+++
title = "Privacy Policy"
description = "We do not use cookies and we do not collect any personal data."
draft = false

[extra]
class = "page single"
+++

__TLDR__: We do not use cookies and we do not collect any personal data.

## Website visitors

- No personal information is collected.
- No information is stored in the browser.
- No information is shared with, sent to or sold to third-parties.
- No information is shared with advertising companies.
- No information is mined and harvested for personal and behavioral trends.
- No information is monetized.

## Contact me

[Contact me](https://gitlab.com/megian/website) if you have any questions.

Effective Date: _4st January 2023_
