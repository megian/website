+++
title = "Megian's Webseite"
description = "Persönliche Website von Megian"
draft = false


# The homepage contents
[extra]
lead = '<b>Megian</b> ist ein pesönliche Webseite'

# Menu items
[[extra.menu.main]]
name = "Docs"
section = "docs"
url = "/docs"
weight = 10

[[extra.menu.main]]
name = "Blog"
section = "blog"
url = "/blog/"
weight = 20

[[extra.list]]
title = "IT Architektur"
content = 'Holen Sie sich verschiedene Kenntnisse über IT-Architektur.'

[[extra.list]]
title = "Samelsurium ⚡️"
content = 'Viele gesammelte Dinge über Jahrzehnte.'

+++
