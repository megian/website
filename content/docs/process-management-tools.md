+++
title = "Process Management Tools"
draft = false
weight = 1
sort_by = "weight"
# date = 2013-09-29 15:00:40+00:00
# updated = 2023-01-05 17:27:50+00:00
+++

* [BPMN Engine](https://www.activiti.org)
* [Yaoqiang BPMN Editor](https://bpmn.sourceforge.net)
