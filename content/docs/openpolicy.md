+++
title = "OpenPolicy - The plan and the theorie"
draft = false
weight = 20
sort_by = "weight"
# date = 2004-06-17 19:34:47+00:00
# updated = 2013-09-27 07:27:50+00:00
+++

## 1. Summary

There where 2 older implementation, but I think, I now have an better soulution to solve my problems. On the way, I find a gut way so save tree-structured datas.

Ideas:

* A realy small implementation (no mem, no diskspace, no cpu, should work on an 386 cpu)
* Types
* Library to read and write datas

## 2. Binary Fileformat

The binary file must be readable in 2d mode.

### 2.1 Types

```
+-----------------+---------------+----------------+
| Main Types      | Data Types    | Attribut Types |
+-----------------+---------------+----------------+
| 0=End           | 16=Binary     | 64=Value       |
| 1=Directory     | 17=Boolean    | 65=ACL         |
| 2=Link          | 18=DWORD      | 66=Modified    |
| 3=External File | 19=String     | 67=Action      |
|                 | 20=Stringlist | 68=Comment     |
+-----------------+---------------+----------------+
```

### 2.2 Format

```
/sys/interface/eth0/address = "192.168.1.2"

+--+--+--+-------+ +--+--+-----------+
|#1|#2|#3|#4     | |#5|#6|#7         |
+--+--+--+-------+ +--+--+-----------+
|18|40|7 |address| |64|10|192.168.1.2|
+--+--+--+-------+ +--+--+-----------+

#1 Main Type or Data Type
#2 Length of the full entry
#3 Length of the Key-String
#4 Key-String
#5 Type of the attribut
#6 Length of the attribut
#7 Attribut
```