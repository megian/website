+++
title = "OpenSource – Freie Software"
description = "Eine Untersuchung zur Frage, wie freie Software benutzt wird?"
draft = false
weight = 1
sort_by = "weight"
# date = 2003-10-10T00:00:00+00:00
+++

Wie wird OpenSource benutzt?
Warum funktioniert das System und was steckt dahinter.
Lesen Sie es!

Download als PDF: [opensource-10.10.2003-1.pdf](opensource-10.10.2003-1.pdf)
Download als Open Dokument: [opensource-10.10.2003-1.odt](opensource-10.10.2003-1.odt)

Download Vortrag als PDF: [svavortrag-22.02.2004-1.pdf](svavortrag-22.02.2004-1.pdf)
Download Vortrag als Open Dokument: [svavortrag-22.02.2004-1.odp](svavortrag-22.02.2004-1.odp)
