+++
title = "LAP Fächerliste"
draft = false
weight = 1
sort_by = "weight"
# date = 2004-03-15 13:45:22+00:00
# updated = 2006-11-04 16:17:59+00:00
+++

## Informatiker

* Allgemeine Grundlagen
    * Naturwissenschaftliche Grundlagen
        * Physik
            * Elektrophysikalische Grundlagen
            * Dynamik
            * Statik
            * Wärmelehre
            * Akustik
            * Optik
        * Chemie
            * Atombau
            * Periodensystem
            * Bindungslehre
            * Redoxreaktion
            * Säuren-Basen-Reaktionen
            * Kunststoffe
            * Giftklasseneinteilung
            * Materialentsorgung, Recycling
    * Betriebswirtschaftliche Grundlagen
        * Finanzbuchhaltung
        * Betriebliches Rechnungswesen
        * Betriebsorganisation
        * Produktionsplanung und -steuerung
        * Betriebstechnik
* Fachtechnische Grundlagen
    * Teil1: Elektrotechnik, Elektronik
        * Elektrotechnik
            * Schaltung von Widerständen
            * Spannungsquellen
            * Leistung, Arbeit, Energie
            * Spannungsfunktionen
            * Elektrisches und magnetisches Feld
            * Elektrische Komponenten
        * Elektronik
            * Messtechnik
            * Halbleitertechnik
            * Digitaltechnik
            * Grundlagen der HF-Technik
            * Grundlagen der Regeltechnik
    * Teil 2: Informatikgrundlagen
        * Grundlagen
        * Technische Unterlagen
        * Organisation und Automatisierung von Tätigkeiten
        * Datentechnik
        * Softwareerstellung
        * Standardsoftware
        * Hardware
        * Betriebssysteme
        * Teleinformatik
        * Prozessinformatik
* Angewandte Fachkenntnisse
    * Teil 1: Informatikanwendungen
    * Teil 2: Technisches Englisch