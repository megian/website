+++
title = "GTK Simple Example"
draft = false
weight = 1
sort_by = "weight"
# date = 2004-05-12 20:12:19+00:00
# updated = 2013-07-23 10:49:36+00:00
+++

```c
#include <gtk/gtk.h>

gint delete_event (GtkWidget *widget, GdkEvent event, gpointer data)
{
  return FALSE;
}

void mainend (GtkWidget *widget, gpointer data)
{
  gtk_main_quit();
}

int main (int argc, char **argv)
{
  GtkWindow *mainwindow;

  gtk_init (&argc, &argv);
  mainwindow = g_object_new (GTK_TYPE_WINDOW, "title", "Simple", NULL);
  g_signal_connect (mainwindow, "delete_event", G_CALLBACK(delete_event), NULL);
  g_signal_connect (mainwindow, "destroy", G_CALLBACK(mainend), NULL);

  gtk_widget_show_all (GTK_WIDGET(mainwindow));
  gtk_main ();

  return (0);
}
```

Makefile:
```
gcc -o simple simple.c `pkg-config gtk+-2.0 --cflags --libs`
```