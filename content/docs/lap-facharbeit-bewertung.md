+++
title = "LAP Facharbeit Bewertung"
draft = false
weight = 1
sort_by = "weight"
# date = 2004-02-14 20:38:59+00:00
# updated = 2006-11-04 16:17:07+00:00
+++

## Fragen

Pro Gebiet je 36 Punkte.

Teil A: Berufsübergreifende Fähigkeiten

|     |     |     |     |
| --- | --- | --- | --- |
| ID  | Leitfrage | Bewertung | Bemerkungen |
| 1   | Projektmanagement (13) | !   | Projektmanagement |
| 2   | Arbeitsmethodik (2) | !   | Was ist richtig! |
| 3   | Wissensbeschaffung (42) |     |     |
| 4   | Leistungsbereitschaft / Einsatz / Arbeitshaltung (7) |     |     |
| 5   | Selbständiges Arbeiten (44) |     |     |
| 6   | Arbeitsplatzorganisation (4) | =   | Ergonomie! |
| 7   | Demo / Vorführung des Produktes der Facharbeit (59) |     |     |
| 8   | Präsentation: Zeitmanagement (48) | !   | genau 15-20 min! |
| 9   | Präsentation: Themenwahl (49) |     |     |
| 10  | Präsentation: Medieneinsatz - Moderationstechniken (8) | !   | Mehrere Medien gefordert! |
| 11  | Präsentation: Blickkontakt und Gestik | !   | 2s pro Person / alle Blickpartner / offen, ruhig, kontrolliert |
| 12  | Präsentation: Lautstärke und Sprechgeschwindigkeit (12) |     |     |

Teil B: Qualität Resultat/Doku (Security/Firewalls/DMZ)

|     |     |     |     |
| --- | --- | --- | --- |
| ID  | Leitfrage | Bewertung | Bemerkungen |
| 1   | Umsetzung (16) |     |     |
| 2   | Vollständigkeit (28) |     |     |
| 3   | Test der Lösung (29) | !   | Protokoll! |
| 4   | Leistungsfähigkeit (30) |     |     |
| 5   | Computer-Virenschutz (154) |     | Inventarisierung der PCs / Auswahl der AntiViren-Programms / Aktuallisierungen sichegestellt & dokumentiert |
| 6   | Wahl der Verteilmethode (164) |     | enspricht Kundenanforderungen |
| 7   | Virenschutz (104) |     | Software-Updates funktionieren / Kontrollfunktion / Ablauforganisation bei Virenvorfällen |
| 8   | Sicherheitsaspekte (165) |     | Sicherheitspatches sind installiert, dokumentiert & kontrolliert |
| 9   | Abläufe mit Scripts/Makros automatisieren (149) |     | nach Aufgabenstellung / funktioniert (testen!) / Planungschritte nachvollziehbar dokumentiert |
| 10  | Installation komplexer Software-Pakete (133) |     | korrekt installiert / dokumentiert / Betriebshandbuch / Firmenstandards + Richtlinien / Lizenz |
| 11  | Windows-Server (155) |     | Sicherheitsstrategie / Sicherheitskontrollen / Freigabe von Verzeichnissen, Benutzerprofile, Protokollierung, Schutz der Registry, Schutz der Administratoren Konten, Installation Patches |
| 12  | Server Virenschutz in einem Netzwerk (168) |     | mehrmals täglich automatisch aktuallisiert / zentrale Alarmierung / Kontrolle möglich / Sporadisch "on demand scans". |

Teil C: Dokumentation

|     |     |     |     |
| --- | --- | --- | --- |
| ID  | Leitfrage | Bewertung | Bemerkungen |
| 1   | Konzeptionelles Denken (17) |     |     |
| 2   | Führung des Arbeitsjournales (18) |     | Vollständig / Lückenlos / Erfolge & Probleme / Hilfestellungen durch Dritte |
| 3   | Reflexionsfähigkeit (19) |     |     |
| 4   | Gliederung (20) |     |     |
| 5   | Prägnanz - Redundanz (21) |     | Keine Redundanzen |
| 6   | Vollständigkeit (23) |     | Inhaltsverzeichnis / Quellenverzeichnis / Druckdatum / Name / Titelblatt |
| 7   | Sprachlicher Ausdruck/Stil (24) |     | Verständlichkeit für Aussenstehende |
| 8   | Darstellung (25) |     | Übersichtlich / zweckmässig / sauber |
| 9   | Rechtschreibung (26) |     | Maximal 10 Rechtschreibfehler! |
| 10  | Grafiken (27) |     |     |
| 11  | Dokumentation des Testverfahren und dessen Resultate (22) |     | für aussenstehende Person klar |
| 12  | Web-Summary (51) |     |     |

Teil D: Fachkompetenz (aus Fachgespräch)

|     |     |     |     |
| --- | --- | --- | --- |
| ID  | Leitfrage | Bewertung | Bemerkungen |
| 1   | Methodeneinsatz (31) |     |     |
| 2   | Wissensweitergabe (32) |     | für Aussenstehende |
| 3   | Zuhören (33) |     | Zuhören nicht Nachfragen! |
| 4   | Anwendung der Fachsprache (34) |     |     |
| 5   | Projektumfeld: Systemgrenzen / Schnittstellen zur Aussenwelt (39) |     | Klare Grenzen ziehen! |

Teil E: Eigene Bemerkungen

|     |     |     |     |
| --- | --- | --- | --- |
| ID  | Leitfrage | Bewertung | Bemerkungen |
| 1   | Datenbanken: Normailiseren | !   | Nicht gelernt, wird in der Praxis nicht angewendet! |
| 2   | Virenschutz |     | Mehrmals täglich updates / Kontrolle / Zentrale stelle melden |


## Notenrechnung

IPA-Note = 5 * ((Teil A)*2 + (Teil B) + (Teil C) + (Teil D) - (Abzug)/180)+1