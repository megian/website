+++
title = "Debian"
draft = false
weight = 20
sort_by = "weight"
+++

# Debian

## Install the Cloud Image

The cloud image is samller and doesn't contain all hardware related modules:
```shell
apt install -y linux-image-cloud-amd64
```

## Reduce Debian

Spired by [ReduceDebian](https://wiki.debian.org/ReduceDebian).

```shell
apt remove --purge autotools-dev autoconf automake autopoint libc6-dev
apt remove --purge discover-data git git-man
apt remove --purge fontconfig-config installation-report perl
apt remove --purge thin-provisioning-tools ttf-bitstream-vera xauth
```
