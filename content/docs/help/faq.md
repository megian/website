+++
title = "FAQ"
description = "Answers to frequently asked questions."
draft = false
weight = 30
sort_by = "weight"
template = "docs/page.html"
# date = 2023-01-04T13:14:00+00:00

[extra]
lead = "Answers to frequently asked questions."
toc = true
top = false
+++

## What is the AdiDoks?

AdiDoks is a Zola theme for Documentation's sites, ported by the Hugo Theme
[Doks](https://getdoks.org), which is a pretty nice theme. Thanks a lot to 
[*h-enk*](https://github.com/h-enk), the creator of the [Doks](https://getdoks.org).

## Keyboard shortcuts for search?

- focus: `/`
- select: `↓` and `↑`
- open: `Enter`
- close: `Esc`


## Contact the creator?

Send *Gabriel Mainberger* an E-mail:

- <gabisoft@freesurf.ch>
