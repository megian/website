+++
title = "Biorhythmus"
draft = false
weight = 1
sort_by = "weight"
# date = 2003-01-01 00:00:00+00:00
# updated = 2023-01-06 22:36:06+00:00
+++

![](biorhythmus.png)

Der [Source Code](https://github.com/megian/biorhythmus) ist auf GitHub zu finden.

Inspiriert von einem DOS Programm: [biorhythmus-de-i386.exe](biorhythmus-de-i386.exe)