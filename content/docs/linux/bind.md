+++
title = "Bind - HOWTO"
weight = 1
sort_by = "weight"
+++

Hier ein kleines HOWTO, wie man den Bind Deamon konfiguriert.
Bind (Berkley Internet Name Domain) ist eine häufig eingesetzte Implementation des DNS protocols und wird vom [ISC](https://www.isc.org) (Internet System Consortium) entwickelt.

Als erstes sollte schon ein Betriebssystem eingerichet sein.
Weiter muss Bind installiert werden, vozugsweise über das Package Management System (bei Debian z.B. `apt-get install bind9`) oder einfach kompilieren.

## named.conf - Zentrales Konfigurationsfile für Bind

```
options {
	directory "/var/named";
	# the default is to fail, if the master file is not correct
	check-names master warn;

	pid-file "/var/run/named.pid";
	#statistics-interval 720;
	#cleaning-interval 720;

	datasize default;
	stacksize default;
	coresize default;
	files unlimited;
	recursion yes;

	multiple-cnames no;		// if yes, then a name my have more
					// than one CNAME RR.  This use
					// is non-standard and is not
					// recommended, but it is available
					// because previous releases supported
					// it and it was used by large sites
					// for load balancing.

	# the default is to listen on port 53 on all available interfaces
	# you can also give a detailed list:

        forward first;

	# +++ DNS forwarders +++
	forwarders {
                193.231.13.232;
                193.231.12.168;
        };

	# +++ Netzwerk anfragen beantworten +++
	listen-on { 127.0.0.1/32; 192.168.1.0/24; };

	# +++ keine unnoetigen pakete! +++
        notify no;

	# +++ clean up only every 12 hours +++
	cleaning-interval 720;
	statistics-interval 720;

};

zone "." IN {
        type hint;
        file "root.hint";
};

zone "localhost" IN {			#  +++ localhost +++
	type master;
	file "localhost.zone";
	check-names fail;
	allow-update { none; };
};

zone "0.0.127.in-addr.arpa" IN {
	type master;
	file "<a href="download/linux/bind/127.0.0.zone">127.0.0.zone</a>";
	check-names fail;
	allow-update { none; };
};

zone "mynet.local" IN { 		#  +++ mynet.local +++
        notify no;
        type master;
        file "mynet.local.zone";
};

zone "1.168.192.in-addr.arpa" IN {
        notify no;
        type master;
        file "192.168.1.zone";
};
```

## 192.168.1.zone - Zone File 1
```

$ORIGIN 1.168.192.in-addr.arpa.

@			IN SOA	server.mynet.local. emailadresse.server.mynet.local. (
					42		; serial (d. adams)
					3H		; refresh
					15M		; retry
					1W		; expiry
					1D )		; minimum

			IN NS	server.mynet.local.

1			   IN PTR	server.mynet.local.
1			   IN PTR	www.mynet.local.
2        1D IN PTR	host1.mynet.local.
```

## mynet.local.zone - Zone File 2

```
$ORIGIN mynet.local.

@                       IN SOA       server.mynet.local emailadresse.server.mynet.local (
                                        42              ; serial (d. adams)
                                        3H              ; refresh
                                        15M             ; retry
                                        1W              ; expiry
                                        1D )            ; minimum

                        IN TXT       "Mein guter Server!"
                        IN NS        server
                        IN MX        10 mailserver

server                  IN A         192.168.1.1
                        IN TXT		 "Server"

www                		IN A         192.168.1.1
                        IN TXT	     "WebServer"

host1                   1D IN A      192.168.1.2
                        1D IN TXT    "Wokstation 1"
```

So, und wenn man die Dateien angepasst hat und den Deamon gestartet hat, sollte man nun DNS Anfragen an den Server schicken können.