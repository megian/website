+++
title = "Linux-Tips"
draft = false
weight = 1
sort_by = "weight"
+++

## Ext2 zu Ext3 konvertieren

Ext3 bringt einige Vorteile. Da alle neueren Linux-Kernel Ext3 unterstützen, kann ohne Probleme konvertiert werden. Da Ext3 auf Ext2 Aufbaut muss nur das Journaling eingeschaltet werden. Dies geschieht mit dem Befehl:

`tune2fs -j /dev/hda1`

Je nach Partition muss natürlich ein anderes Device angegeben werden. Damit der Kernel die Partition nun auch als ext3 mountet, muss dies noch in der `/etc/fstab` angegeben werden.

## A4 scannen und als PDF speichern

`scanimage -d snapscan:/dev/usb/scanner0 --mode Lineart --resolution auto -x 210 -y 294 | pnmtops | ps2pdf - fertig.pdf`

## Packages.gz erstellen

`dpkg-scanpackages . /dev/null "" < Packages   gzip > Package < Packages.gz`

## /dev/lp0 zurücksetzen

Bei gewissen Druckern ergibt sich ein Problem, wenn man sie während dem Drucken ausschaltet und wieder einschaltet. Da der Paralellport immer noch Daten im Buffer hat, gibt der Drucker sinnlose Zeichen aus. Folgender Befehl löscht den Buffer:

`cat /dev/zero > /dev/lp0`

## PI berechnen

```
#include <stdio.h>
int a=10000,b,c=2800,d,e,f\[2801\],g;main(){for(;b-c;)f\[b++\]=a/5;for(;d=0,g=c\*2;c-
=14,printf("%.4d",e+d/a),e=d%a)for(b=c;d+=f\[b\]\*a,f\[b\]=d%--g,d/=g--,--b;d\*=b)
;}
```

## VMPlayer Linux - Kernel Module kompilieren

`sudo vmware-modconfig --console --install-all`

## Apt-get & dpkg

Apt-get & dpkg von Debian sind wirklich geniale Tools, wenn man weiss, wie man sie einsetzt.

Paket installieren: apt-get install <package>\
Package löschen: apt-get remove <package>\
Package und Konfigurationsdateien löschen: apt-get remove --purge <package>\
Package suchen: apt-cache search <stichwörter>\
Packageinformationen anzeigen: apt-cache show <package>\
                               oder dpkg -s apt-cache search <stichwörter>\
In welchem Package ist welche Datei: dpkg -S <datei>\
Dateilisting eines Packages: dpkg -L <package>