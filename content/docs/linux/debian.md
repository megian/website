+++
title = "Debian"
draft = false
weight = 1
sort_by = "weight"
+++

## Einführung

"Debian ist eine weltweit verteilte Gemeinschaft, welche sich für die Freiheit der Software einsetzt."
So kann man [Debian](https://www.debian.org) am besten mit einem Satz erklären.
Damit stellt sich natürlich die Frage, ob Software allgemein gefärdet ist.
Und aus Sicht der Debian Gemeinschaft ist sie es, da sie Algorithmen als Allgemeingut sieht.
So wie es die Mathematik oder die Sprache ist.

Durch den Schutz des Quellcodes bei Software, gehen Jahrelange Forschungen in den Besitz einzelner Firmen über.
Die Algorithmen, werden von diesen Patentrechtlich geschützt, so das diese nicht mehr verwendet werden dürfen.
Dies verlangsamt die komplette Entwicklung der Informatik und fördert in hohem Masse, schlechte Lösungen.

Die Informatik ist kurzlebig, wenn also ein Konzept, was vor über 30 Jahren entwickelt und umgesetzt worden ist, immer noch existiert, muss es grundlegende Eingeschaften haben.
Dazu gehören, das hirarchische Dateisystem, die Datei Routinen (open, read, write, close), die Kommunikation über Sockets (TCP/IP), sowie Funktionen wie Multitaking und Multiuserfähigkeit.

Was ist passiert, das sich Unix vorerst nicht durchsetzen konnte? Es war proprietär oder anarchisch.
Entweder war das Unix im Besitz einer Firma oder es wurde unter einer BSD Lizenz verteilt.
Beide Wege, waren nicht besonders erfolgreich und konnten sich nicht durchsetzen.

Linux geht mit der GPL Lizenz einen Weg der eingeschränkten Freiheit.
Zwar ist es immer noch so frei verfügbar wie BSD, aber jedermann ist gezwungen, die Änderungen, wieder verfügbar zu machen.
Dadurch erhält Linux eine ziemlich stabile Struktur.

## Der Linux Kernel

Der Name Linux wird vielfach als Bezeichnung für das Betriebssystems verwendet, was eigentlich falsch ist, da es nur der Name des Kernel ist.
Da sich aber dieses Missverständnis so verbreitet hat, kann man heute ruhig auch Linux sagen, wenn es um die Distribution geht.

Hauptentwickler von Linux ist immer noch Linus Torvalds, welcher als oberster Richter gilt und Code entweder annimmt oder ablehnt.
Er hat 1991 Linux als Terminalprojekt begonnen, um seine E-Mails lesen zu können.

Dazu hat er grundlegende Systemroutinen programmieren müssen und somit bald ein minimales Betriebssystem entwickelt.
Dieses wurde immer umfangreicher und er veröffentlichte eine erste Version (Er wollte es Freax nennen, aber seine Freunde nannten es einfach Linux).
Sie konnte bereits einige GNU Tools, sowie den GCC ausführen.
Auch wenn nun fortan, sehr viele Entwickler an Linux gearbeitet haben, dauerte es noch lange bis ein erster TCP/IP Stack implementiert war. Dieser war unabdingbar, da die Entwickler weltweit verteilt sind und somit eine solche gemeinsame Entwicklung ohne Netzwerk unmöglich gewesen wäre.