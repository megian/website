
+++
title = "GRUB Installation mit LiveCD"
draft = false
weight = 1
sort_by = "weight"
# date = 2005-05-22 20:42:29+00:00
# updated = 2006-11-04 17:22:12+00:00
+++

```bash
su -
mount -t ext4 /dev/sda3 /mnt
cd /mnt
chroot /mnt
mount -t ext4 /dev/sda1 /boot
grub-install /dev/sda
```