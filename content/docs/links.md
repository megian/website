+++
title = "Links"
draft = false
weight = 1
sort_by = "weight"
# date = 2023-01-23 20:48:00+00:00
# updated = 2023-01-23 20:48:00+00:00
+++

# Art

* [CGSamurai.ch](https://www.cgsamurai.ch)

# PC News

* [heise.de](https://www.heise.de)
* [golem.de](https://www.golem.de)
* [pro-linux.de](https://www.pro-linux.de)
* [holarse.de - Linux Game Page](https://holarse.de)
* [ZDnet](https://www.zdnet.com/)
* [Windows Networking](https://techgenix.com/networking/)
* [Network World](https://www.networkworld.com)

# Linux
* [Kernel.org - Linux Kernel Source](https://www.kernel.org)
* [Linux Kernel 2.6 - deutsche Übersetzung des "The post-halloween document"](https://www.kubieziel.de/computer/halloween-german.html)
* [Debian.org](https://www.debian.org)
* [Knoppix](https://www.knoppix.de)
* [Dell Linux Page](https://linux.dell.com)
* [Linux Printing](https://openprinting.github.io)
* [GamingOnLinux](https://www.gamingonlinux.com)

# Programmierung
* [GTK](https://www.gtk.org)
* [Black Belt Coder](http://www.blackbeltcoder.com)

# OpenSource
* [OpenSource.org](https://www.opensource.org)
* [OpenSource - Lizenzen](https://www.opensource.org/licenses/)
* [Opensource.com](https://www.opensource.com)
* [The Linux Foundation](https://www.linuxfoundation.org)
* [The Cathedral and the Bazaar](http://www.catb.org/~esr/writings/cathedral-bazaar/)
* [Free Software Foundation Europe](https://fsfe.org/index.de.html)
* [OpenSource Mythen](https://www.neilgunton.com/open_source_myths/)

# Microsoft
* [Microsoft Leute haben keine Ahnung von \\n](https://blogs.msdn.microsoft.com/brada/2004/08/08/pet-peeve-493-console-writeline-n/)

# Projekte
* [Blender - 3D Grafikprogramm](https://www.blender.org)
* [7-Zip](https://7-zip.org)
* [Celestia - 3D Weltraumsimulation](https://celestia.space)
* [Mozilla Browser](https://www.mozilla.org)
* [Syslinux](https://www.syslinux.org)
* [ntpasswd](https://pogostick.net/~pnh/ntpasswd/)

# Software
* [Windows Sysinternals](https://learn.microsoft.com/de-ch/sysinternals/)

# Hardware
* [PCI Database](https://pci-ids.ucw.cz)
* [Computerbase](https://www.computerbase.de)
* [FCCID](https://www.fcc.gov/oet/ea/fccid)
* [SYSOPT](https://www.sysopt.com)
* [Overclockers](https://www.overclockers.com)
* [Storage Review](https://www.storagereview.com)
* [Techreport](https://techreport.com)
* [USB Memo](https://www.techwriter.de/thema/usb-memo.htm)
* [DVD FAQ](https://www.dvddemystified.com/dvdfaq.html)

# Wissenschaft
* [Periodensystem](https://www.periodensystem.info)
* [Synonym-Finder](https://www.synonym.com/synonym/)
* [Englisch Gramatik](https://www.englisch-hilfen.de/grammar/tabelle_zeit.htm)
* [WolframAlpha](https://www.wolframalpha.com/)

# Wirtschaft
* [Chris Martenson - Peak Prosperity](https://peakprosperity.com/)
* [ADEV Energiegenossenschaft](https://www.adev.ch)
* [Wie Dropbox skalierte"](https://techcrunch.com/2013/07/11/how-did-dropbox-scale-to-175m-users-a-former-engineer-details-the-early-days/)

# Tips
* [TED](https://www.ted.com)
* [SELFHTML](https://www.selfhtml.org)
* [Clipart.com](https://www.clipart.com)
* [IBM Redbooks](https://www.redbooks.ibm.com/)

# Browser Tests
* [Browserspy](https://browserspy.dk)
* [Meyerweb.com](https://meyerweb.com/eric/css/edge/)
* [animieres PNG](https://meyerweb.com/eric/css/edge/)
* [Offizielle CSS Seite](https://www.w3.org/Style/CSS/)
* [CSSzen Garden](https://www.csszengarden.com)

# Hersteller
* [Soekris - Mini PC Hardware](http://www.soekris.com)
* [NoMachine - NX](https://www.nomachine.com)

# Übersetzer
* [dict.leo.org - LEO Dictionary (Englisch)](https://dict.leo.org)
* [Google Translator](https://translate.google.com)
* [Währungsumrechner](https://www.oanda.com/currency/converter)
* [MAC Adressen / Hersteller](https://www.coffer.com/mac_find/)

# Fun
* [Welches Betriebsystem bist du?](https://www.bbspot.com/News/2003/01/os_quiz.php)
* [Fachbegriffe der Informatik](http://altlasten.lutz.donnerhacke.de/mitarb/lutz/usenet/Fachbegriffe.der.Informatik.html)
* [Experimental-Chemie](https://www.experimentalchemie.de)
* [Microsoft Developer](http://www.ntk.net/media/developers.mpg)
* [Mars Ratten](https://mars.nasa.gov/mer/gallery/all/2/p/006/2P126912441EDN0205P2218L5M1.JPG)
* [Programmierer Song](https://www.ravn.de/stuff/Programmierer.mp3)
* [My Old Mac](http://myoldmac.net/webse-e.htm)
* [Microsoft Bob](https://en.wikipedia.org/wiki/Microsoft_Bob)
* [Stare zwitschern Ursonate - und stellen damit das Urheberrecht in Frage](https://www.telepolis.de/features/Stare-zwitschern-Ursonate-und-stellen-damit-das-Urheberrecht-in-Frage-3449395.html)
* [Microsoft API war](https://www.joelonsoftware.com/2004/06/13/how-microsoft-lost-the-api-war/)
* [Reset.ch](https://www.reset.ch)
* [Darwin Awards](https://darwinawards.com/)

# Homepages
* [Winterhart.ch](https://www.winterhart.ch)

# BIOS and Drivers
* [Tweak PC](https://www.tweakpc.de/)

# Internet
* [Internet Traffic Report](http://www.internettrafficreport.com)

# Hack
* [L0pht](https://l0pht.com)

# Life
* [Entspannung - TheF**kitLife](https://www.thefuckitlife.com/)
* [Positive News](https://www.positive.news/)
