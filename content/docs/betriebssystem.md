+++
title = "Betriebssystem"
draft = false
weight = 1
sort_by = "weight"
# date = 2004-03-26 09:07:31+00:00
# updated = 2006-11-04 16:19:10+00:00
+++

Das Betriebssystem ist ein Software-Abstraktionschicht, welche für Anwendungs-Programme eine Emulationschicht (Systemaufrufe) bildet.

* Betrirebssystem: Prozessorverwaltung, Speicherverwaltung, Betriebsmittelverwaltung
* Kernel: Zugriff auf Hardware
* Anwendungsschicht: Dienstprogramme
* Benutzerschicht: Benutzerprogramme
* Betriebsmittel: Hardware

Die Anwendungssoftware benutzt die Systemaufrufe, um über das Betriebssystem die Hardware anzusprechen.
Direkt Zugriffe sind nicht erlaubt und wären eine Gefahr für die Systemstabilität.

## BIOS

Das BIOS (Basic Input Output System) ist eine Sammlung kleiner grundlegender Funktionisbibliothekten, welche für ein bestimmtes Motherboard ausgelegt sind.

* Mit diesen Programmen wird das Aufstarten des PC-Systems geregelt.
* Weiter regelt das Betriebssystem den Datenaustausch zwischen den verschiedenen Geräten im PC.

Inhalt:
* POST (Power On Self Test): Test-Routinen
* Setup: Verwalten der BIOS Einstellungen (CMOS)
* BIOS: Routinen für den Zugriff auf die Hardware
* Bootstrap: Laden des Betriebssystems

Boot Sequenz:

1.  POST
2.  Setup
3.  Bootstrap

BIOS Hersteller:

* AMI (American Megatrends)
* Phoenix
* Award

CMOS:

* CMOS: Complimentary Metal Oxide Semicondutor
* 100 bis 200 Bytes Speicher
* RAM gestützt durch Batterie

## Prozesse

Der Prozessor führt nacheinander verschiendene Prozesse aus.

Prozess Statuse Linux:

* S: Sleeping
* SW: Swapped
* R: Running
* Z: Zombie (Prozess tot, keine Rückmeldung an Vaterprozess)
* T: Terminated

Prozesstabelle: /usr/include/linux/sched.h
