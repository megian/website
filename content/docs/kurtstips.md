+++
title = "Kurztips"
draft = false
weight = 1
sort_by = "weight"
# date = 2004-05-13 08:28:58+00:00
# updated = 2013-07-23 11:50:06+00:00
+++

## Lichtwellen

c = λ * f

- c = Lichtgeschwindigkeit
- λ = Wellenlänge
- f = Frequenz