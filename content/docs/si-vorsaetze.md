+++
title = "Elektrotechnik: SI-Vorsätze (DIN 1301)"
draft = false
weight = 20
sort_by = "weight"
# date = 2004-02-14 20:38:59+00:00
# updated = 2006-11-04 16:17:43+00:00
+++

|     |     |     |     |
| --- | --- | --- | --- |
| Vorsatz-  <br>zeichen | Vorsatz-  <br>benennung | Faktor | Beispiele |
| a   | Atto | 10-18 | 1 Attometer = 1 am = 0.000 000 000 000 000 001 m |
| f   | Femto | 10-15 | 1 Femtometer = 1 fm = 0.000 000 000 000 001 m |
| p   | Piko | 10-12 | 1 Pikometer = 1 pm = 0.000 000 000 001 m |
| n   | Nano | 10-9 | 1 Nanometer = 1 nm = 0.000 000 001 m |
| μ   | Mikro | 10-8 | 1 Mikrometer = 1 μm = 0.000 001 m |
| m   | Milli | 10-3 | 1 Millimeter = 1 mm = 0.001 m |
| c   | Zenti | 10-2 | 1 Zentimeter = 1 cm = 0.01 m |
| d   | Dezi | 10-1 | 1 Dezimeter = 1 dm = 0.1 m |
| da  | Deka | 101 | 1 Dekameter = 1 dm = 10 m |
| h   | Hekto | 102 | 1 Hektometer = 1 hm = 100 m |
| k   | Kilo | 103 | 1 Kilometer = 1 km = 1000 m |
| M   | Mega | 106 | 1 Megameter = 1 Mm = 1000 000 m |
| G   | Giga | 109 | 1 Gigameter = 1 Gm = 1000 000 000 m |
| T   | Tera | 1012 | 1 Terameter = 1 Tm = 1000 000 000 000 m |
| P   | Peta | 1015 | 1 Petameter = 1 Pm = 1000 000 000 000 000 m |
| E   | Exa | 1018 | 1 Exameter = 1 Em = 1000 000 000 000 000 000 m |