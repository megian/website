+++
title = "Datenbanken"
draft = false
weight = 1
sort_by = "weight"
# date = 2004-03-30 10:44:43+00:00
# updated = 2006-11-04 17:32:55+00:00
+++

## Elemente

### Entität

Eine Entität ist ein indviduelles und identifizierbares Exemplar einer Sache, einer Person oder eines Begriffs aus der realen oder gedachten Vorstellungswelt, das abgebildet werden soll.

### Kernentität

Eine eindeutig identifizierbare Einheit.
Eine Einheit, deren Existenz auf einem geeigneten Speichermedium aufgrund eines Identifikationsmerkmals unabhängig von der Existenz einer anderweitigen Entität darstellbar sein muss.

### Eigenschaft

Eine Eigenschaft wird Entitäten zugeordnet und ermöglicht damit deren Charakteristisierung, Klassierung und Identifizierung.
Die Eigenschaft hat einen Namen und einen Eigenschaftswert.

### Faktum

Ein Faktum ist eine Behauptuung, derzufolge eine Entität für eine Eigenschaft einen bestimmten Eigenschaftswert aufweist.

### Beziehung

Eine Beziehung assoziiert wechselseitig zwei (möglicherweise auch mehr) Entitäten.

### Entitätsmenge

Eine Entitätsmenge ist eine eindeutig benannte Kollektion von Entitäten gleichen Typs.

### Domäne

Eine Domäne stellt eine eindeutig benannte Kollektion der zulässigen Eigenschaftswerte einer Eigenschaft dar.

### Beziehungsmenge

Eine Beziehungsmenge ist eine eindeutig bekannte Kollektion von Beziehungselementen gleichen Typs.

### Attribut

= Feld

### Tupel

= Record
= Datensatz

## Notationen

genau ein = 1
kein oder ein = c
ein oder mehrere = m
kein, ein oder mehrere = mc

## Datenmodelierung

### Bauteile

* Einfach-Mehrfach Beziehung: 1: mc
* Beziehungsmenge: 1:mc mc:1
* Bedingte Auslagerung: 1:c
* Hirarchie: mc:c
* Stückliste: 1:mc/1:mc

### Verboten

* 1:1
* mc:mc

## Normalisierung

Die Normalisierung sollte in der Praxis vermieden werden, da es nicht die beste Lösung hervorbringt.

Die Normalisierung ist eine Methode, von unnormalisierten Relationen über mehrere Schritte in eine vollnormalisierte Relation zu gelangen.

### unnormalisierte Form

In einer unnormalisierten Relation sind Mengen als Attribute zulässig.
Dies bedeutet, dass am Kreuzungpunkt einer Kolonne (attribut) und einer Zeile (Tupel/Datensatz) unter Umständen eine Menge von Elementen vorzufinden ist.

### 1. Normalform

In einer in 1. Normalform befindlichen Relation sind nur einfache (also keine mengenmässigen Attributwerte zulässig.
Dies bedeutet, dass am Kreuzungspunkt einer Kolonne (Attribut) und einer Zeile (Tupel/Datensatz) jederzeit höchstens ein Element vorzufinden ist.

### 2. Normalform

Eine in 2. Normalform befindliche Relation ist dadurch gekennzeichnet, dass jedes nicht dem Schlüssel Aangehörende Attribut funktional abhängig ist vom Gesamtschlüssel, nicht aber von Schlüsselteilen.

Eine Relation ist in 2. Normalform, falls sie die 1. Normalform respektiert und jedes nicht dem Schlüssel angehörende Attribut vom Schlüssel voll funktional abhängig ist.

### 3. Normalform

Eine in 3. Normalform befindliche Relation ist dadurch gekennzeichnet, dass jedes nicht dem Schlüssel angehörende Attribut funktional ist vom Gesamtschlüssel, nicht aber von einzelnen Schlüsselteilen.
Ferner sind keine funktionalen Abhängigkeiten zwischen Attributen erlaubt, die nicht als Schlüsselkandidaten in Frage kommen.

Eine Relation ist in 3. Normalform, falls die in 2. Normalform und keine transitiven Abhängigkeiten aufweist.