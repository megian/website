+++
title = "LAP: Zusammenfassungen"
draft = true
weight = 20
sort_by = "weight"
# date = 2004-05-29 21:58:54+00:00
# updated = 2006-11-04 15:20:32+00:00
+++

* [Physik](/files/Physik-30.5.2004-1.odt) [(PDF)](/files/Physik-30.5.2004-1.pdf)
* [Physik Formeln](/files/Formelnphys.doc) [(PDF)](/files/Formelnphys.pdf)
* Chemie ([DOC](/files/Chemie.doc) [PDF](/files/Chemie.pdf))
* [Elektrotechnik](/files/Elektrotechnik-30.05.2004-1.odt) [(PDF)](/files/Elektrotechnik-30.05.2004-1.pdf)
* [Elektronik](/files/Elektronik-30.05.2004-1.odt) [(PDF)](/files/Elektronik-30.05.2004-1.pdf)
* [BWL BAB](/files/bwl-bab-28.5.2004-1.ods) [(PDF)](/files/bwl-bab-28.5.2004-1.pdf)
* [BWL Break-even](/files/bwl-break-even-27.5.2004-1.ods) [(PDF)](/files/bwl-break-even-27.5.2004-1.pdf)
* BWL Aufgaben Lösung ([ODT](/files/BWL_Aufgaben_Loesungen_2.06.04.odt) [PDF](/files/BWL_Aufgaben_Loesungen_2.06.04.pdf))
* BWL Aufgaben Lösung ([ODT](/files/BWL-Loesungen2-08.06.2004-1.odt) [PDF](/files/BWL-Loesungen2-08.06.2004-1.pdf))
* [Physik: Farben](/files/physik-farben-30.5.2004-1.odt) [(PDF)](/files/physik-farben-30.5.2004-1.pdf)
* [Physik-Aufgaben](/files/Physik-Aufgaben-30.05.2004-1.odt) [(PDF)](/files/Physik-Aufgaben-30.05.2004-1.pdf)
* [Regler-Typen](/files/Regler-Typen-30.5.2004-1.odt) [(PDF)](/files/Regler-Typen-30.5.2004-1.pdf)
* [Elektrische-Bauteile](/files/Elektrische-Bauteile-31.5.2004-1.odt) [(PDF)](/files/Elektrische-Bauteile-31.5.2004-1.pdf)
* [Tabellenbuch-Seitenzahlen](/files/Tabellenbuch-Seitenzahlen-08.06.2004-1.odt) [(PDF)](/files/Tabellenbuch-Seitenzahlen-08.06.2004-1.pdf)
* [Datenbanken](/files/Datenbanken.doc)
* [Telematik](/files/telematik.doc)
* Englisch ([1](/files/english_voci_page_1.pdf), [2](/files/english_voci_page_2.pdf), [3](/files/english_voci_page_3.pdf), [Übersetzung](/files/englisch_core_vocabulary_uebersetzt.pdf))
* Allgemeinbildung - Wie ensteht ein Gesetz? ([PDF](/files/abu-wieenstehteingesetz-2.6.2004-1.pdf))
* [Strukturen](/files/strukturen-2.6.2004-1.odt) [(PDF)](/files/strukturen-2.6.2004-1.pdf) \- Netzplan, Struktogramm, Datenfluss, Programablaufplan, Baumstruktur
* Englisch - Zeitformen [(PDF)](/files/Englisch-Zeitformen-2.6.2004-1.pdf)

Für die, welche immer noch Spass verstehen: [Lehrlings Leben](/files/lehrlingsleben-2.6.2004-1.pdf)

**Danke an Witti für:** Datenbank, Telematik und Englisch Voci.

**Hinweis:** ODT und ODS sind OpenOffice/LibreOffice Dateien.