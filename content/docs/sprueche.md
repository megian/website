+++
title = "Sprüche"
draft = false
weight = 20
sort_by = "weight"
# date = 2004-05-29 22:17:44+00:00
# updated = 2006-11-04 16:20:44+00:00
+++

It is not the lines of code you deliver what matters. What matters is the lines of correct, working code you deliver.

The source will be with you!

Leute die Unix nicht kennen, sind gezwungen es neu entwickeln, nur schlechter.

Unix ist nicht benutzerunfreundlich, es sucht sich aber seine Benutzer aus.