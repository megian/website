+++
title = "CMD Commands"
draft = false
weight = 1
sort_by = "weight"
# date = 2004-05-20 09:08:20+00:00
# updated = 2006-11-04 17:20:16+00:00
+++

|     |     |
| --- | --- |
| ASSOC | Zeigt die Zuordnungen der Dateierweiterungen an oder ändert sie. |
| AT  | Plant die Ausführung von Befehlen und Programmen auf einem Computer. |
| ATTRIB | Zeigt Dateiattribute an oder ändert sie. |
| BREAK | Schaltet Überwachung für STRG+C ein oder aus. |
| CACLS | Zeigt die Zugriffskontrolllisten der Dateien und Verzeichnisse an und ändert sie. |
| CALL | Ruft ein Stapelverarbeitungsprogramm von einem anderen aus auf. |
| CD  | Wechselt das aktuelle Verzeichnis oder zeigt dessen Nummer an. |
| CHCP | Wechselt die aktuelle Codeseite oder zeigt dessen Nummer an. |
| CHDIR | Wechselt das aktuelle Verzeichnis oder ändert es. |
| CHKDSK | Überprüft einen Datenträger und zeigt einen Statusbericht an. |
| CLS | Löscht den Bildschirminhalt |
| CMD | Startet eine neue Instanz des Windows 2000-Befehlsinterpreters. |
| COLOR | Legt die Standardfarben für den Konsolehinter- und Vordergrund fest. |
| COMP | Vergleicht den Inhalt zweier Dateien. |
| COMPACT | Zeigt die Komprimierung der Daten auf NTFS-Partitionen an und ändert sie. |
| CONVERT | Konvertiert FAT-Datenträger in NTFS. |
| COPY | Kopiert Dateien. |
| DATE | Zeigt das Datum an oder wechselt es. |
| DEL | Löscht eine oder mehrere Dateien. |
| DIR | Zeigt Dateien an. |
| DISKCOMP | Vergleicht den Inhalt zweier Disketten. |
| DISKCOPY | Kopiert den Inhalt einer Diskette auf eine Andere. |
| DOSKEY | Bearbeitet Befehlseingaben, ruft Befehle zurück und erstellt Makros. |
| ECHO | Zeigt Meldungen an oder schaltet die Befehlsanzeige ein/aus. |
| ENDLOCAL | Beendet die Begrenzung des Gültigkeitsbereiches von Änderungen. |
| ERASE | Löscht eine oder mehrere Dateien. |
| EXIT | Beendet CMD.EXE. |
| FC  | Vergleicht Dateien oder Zeilen darin. |
| FIND | Sucht in einer oder mehreren Dateien nach einer Zeichenfolge. |
| FINDSTR | Sucht nach Zeichenketten in Dateien. |
| FOR | Führt einen Befehl für jede Datei eines Satzes von Dateien aus. |
| FORMAT | Formatieren von Datenträgern. |
| FTYPE | Zeigt die Dateitypen an, die bei den Dateierweiterungszuordnungen verwendet werden, oder ändert sie. |
| GOTO | Setzt die Ausführung eines Stapelverarbeitungsprogramms an einer Marke fort. |
| GRAFTABL | Ermöglicht Windows 2000, im Grafikmodus einen erweiterten Zeichensatz anzuzeigen. |
| HELP | Zeigt Hilfe für CMD Commands an. |
| IF  | Verarbeitet Ausdrücke mit Bedingungen in einem Stapelverarbeitungsprogramm. |
| LABEL | Erstellt, ändert oder löscht die Bezeichnung eines Datenträgers. |
| MD  | Erstellt ein Verzeichnis. |
| MKDIR | Erstellt ein Verzeichnis. |
| MODE | Konfiguriert Geräte im System. |
| MORE | Zeigt Daten Seitenweise an. |
| MOVE | Verschiebt Dateien. |
| PATH | Legt den Suchpfad für ausführbare Dateien fest. |
| PAUSE | Hält die Ausführung einer Stapelverarbeitungsdatei an. |
| POPD | Wechselt zu einem Verzeichnis welches durch PUSHD gespeichert wurde. |
| PRINT | Druckt Textdateien. |
| PROMPT | Modifiziert den Prompt. |
| RD  | Löscht ein Verzeichnis. |
| RECOVER | Stellt von einem beschädigten Datenträger lesbare Dateien wieder her. |
| REM | Kommentare |
| REN | Benennt Dateien um. |
| RENAME | Benennt Dateien um. |
| REPLACE | Ersetzt Dateien. |
| RMDIR | Löscht ein Verzeichnis. |
| SET | Setzt Umgebungsvaribalen. |
| SETLOCAL | Ändert den Gültigkeitsbereich von Änderungen. |
| SHIFT | Verändert die Position ersetzbarer Parameter in einem Stapelverarbeitungsprogramm. |
| SORT | Sortiert Daten. |
| SUBST | Weist einem Pfad eine Laufwerksbezeichnung zu. |
| START | Startet Programme. |
| TIME | Zeigt die Zeit an oder ändert sie. |
| TREE | Zeigt die Verzeichnisstrukturen grafisch dar. |
| TYPE | Zeigt den Inhalt einer Datei. |
| VER | Zeigt die Nummer der CMD.EXE an. |
| VERIFY | Kontrolliert die Dateien auf die korrekte Speicherung auf einem Datenträger. |
| VOL | Zeigt die Bezeichnung und Seriennummer eines Datenträgers an. |
| XCOPY | Kopiert Dateien und Verzeichnistrukturen. |