+++
title = "Random Hostnames"
draft = true
weight = 1
sort_by = "weight"
# date = 2019-09-29 15:00:40+00:00
# updated = 2019-09-29 17:27:50+00:00
+++

```php

<?php

/*

https://en.wikipedia.org/wiki/Naming_scheme
https://namingschemes.com/
https://tools.ietf.org/html/rfc1178
https://tools.ietf.org/html/rfc2100
https://support.microsoft.com/en-us/kb/909264

*/

function random_char (int $num)
{
	$ret = "";
	$letters = range ('a','z');

	for ($i = 0; $i < $num; $i++)
	{
		$ret = $ret . $letters [mt_rand (0, count ($letters) - 1)];
	}

	return $ret;
}

function random_name_char (string $prefix, int $count, int $num)
{
	$name = $prefix;

	for ($i = 0;$i < $count;$i++)
	{
		$name = $name . "-";
		$name = $name . random_char ($num);
	}

	echo $name . " (" . strlen ($name) . ")<br>";
}


function random_num (int $num)
{
	$ret = "";
	$letters = range('0','9');

	for ($i = 0; $i < $num; $i++)
	{
		$ret = $ret . $letters [mt_rand (0, count ($letters) - 1)]; 
	}

	return $ret;
}

function random_name_num (string $prefix, int $count, int $num)
{
	$name = $prefix;

	for ($i = 0;$i < $count;$i++)
	{
		$name = $name."-";
		$name = $name.random_num ($num);
	}

	echo $name . " (" . strlen ($name) . ")<br>";
}

random_name_num ("o", 4, 2);
random_name_num ("o", 3, 2);
random_name_num ("o", 2, 2);
random_name_num ("o", 2, 3);
random_name_num ("o", 3, 3);

random_name_char ("o", 2, 2);
random_name_char ("o", 3, 2);
random_name_char ("o", 4, 2);
random_name_char ("o", 3, 3);
random_name_char ("o", 5, 2);

?>
```