+++
title = "Gnome screenshots in 2003"
description = "How the Gnome desktop desktop did look like in 2003."
date = 2003-01-01T09:19:42+00:00
updated = 2003-01-01T09:19:42+00:00
draft = false
template = "blog/page.html"

[taxonomies]
authors = ["Gabriel Mainberger"]

[extra]
lead = "An history example how the Gnome desktop did present itself in 2003. Good old times where floppies still did exist. CD-ROMs still had been a common way to deliver software."
+++

The Debian system itself has been installed in 2001 via ISDN. It took a weekend to download all the packages.

{{ gallery() }}