+++
title = "Blog - 19.8.2004 👋"
description = "Seamonkey und SCO vs. IBM"
date = 2004-08-19 19:33:53+00:00
updated = 2006-11-04 15:21:34+00:00
template = "blog/page.html"
draft = true

[taxonomies]
authors = ["Gabriel Mainberger"]

[extra]
lead = 'Aktuelle News'
images = []
+++

Heute ist wieder einmal ein Release von Mozilla's Seamonkey Browser herausgekommen.
Und zwar die dritte Alpha Version von Mozilla 1.8.
Neben den 400 Bugfixes gibt es noch eine intressante Neuerung, das document.all JavaScripts funktionieren sollen.
Meiner Meinung nach sollte das Verboten werden, aber Mozilla kanns halt jetzt auch.

SCO vs. IBM: Was passiert, wenn man die GPL nicht annimmt?
Ganz klar, man darf die Software nicht verändern und weitergeben.
Es greift so das Urheberrecht des Authors.
Blöderweise hat SCO Linux verkauft und so auch Code von IBM mitverkauft.
Sie können jetzt entweder die Anklage weiterziehen und behaupten die GPL sei nicht gültig, aber hiermit würde IBM's Urheberrecht greifen.
SCO wäre dann einfach ein Raupkopierer in grossem Mass.
Es scheint so als wäre SCO tot.
Selbstmord.