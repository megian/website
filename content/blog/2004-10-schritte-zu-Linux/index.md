+++
title = "Zehn Schritte zu Linux"
description = "Der Weg in eine andere Welt..."
date = 2004-09-19T17:20:26+00:00
updated = 2004-09-19T20:01:55+00:00
draft = false
template = "blog/page.html"

[taxonomies]
authors = ["Gabriel Mainberger"]

[extra]
lead = "Der Weg in eine andere Welt..."
+++

![Tux](tux.png)

# 1. Schritt: Back to the roots

* Linux wurde im Sinne der Unix Prinzipien entwickelt, welche über 30 Jahre alt sind.
* Diese Prinzipien sind Informatik Grundlagen.
* Dies bedeutet, das der Computer über den Benutzer gestellt wird.
* Man erhält ein „benutzerunfreundliches“ Betriebssystem, bei dem man aber noch versteht, was das System tut.
  Also doch ein benutzerfreundliches System.

# 2. Schritt: Die Kultur verstehen

* Linux ist Open Source.
  Der Quellcode ist frei.
  Er darf betrachtet, verbreitet und verändert werden, sofern er wieder veröffentlicht wird.
* Linux Programmen werden fast ausschliesslich von Freiwilligen entwickelt.
  Es wird entwickelt, was gebraucht wird oder was Spass macht.
* In diesem Sinne ist Linux Kultur.
  Es ist eine Kultur der Informationsgesellschaft.
  Und Kultur sollte allen zugänglich sein.

# 3. Schritt: Die Einheit

* OpenSource ist ein komplexes Gebilde, welches von hundertausenden Personen gebildet wird.
* Es ist ein System der Handelnden: Wer handelt bestimmt.
* So gibt es keine Führung.
  Und nur wenige Personen werden überhaupt gehört.
* Geführt werden nur die Projekte.
  So, wie die Firmen in einem Staat.

# 4. Schritt: Die Gesetze

* In einem Umfeld, welcher niemand entscheiden kann, müssen klar definierte Standards gelten.
* Falls vorhanden, werden die Standards in Open Source Programmen genutzt.
* Die Entwickler der verschiedenen Projekte entscheiden, welcher Qellcode aufgenommen wird und welcher nicht.
* Jedem steht frei eigene Projekte zu starten.

# 5. Schritt: Keine Grenzen

* OpenSource Programme haben keine Grenzen.
  Jeder darf portieren, weiterentwickeln und verbessern.
* So läuft der Linux Kernel auf 11 Architekturen, vom PDA bis zum Cluster und auch in Emulationen.
* Alles ist Modular und austauschbar.

# 6. Schritt: Linux in Firmen

* Hier prallen Kulturen aufeinander.
  Ein System, welches in erster Linie zur Bedingung hat, die Kultur zu erhalten, soll funktional dienen.
* Firmen haben Geschäftsgeheimnisse und Patente, welche nicht mit Open Source vereinbar sind.
* Es wird mit Linux Geld gemacht, aber die Entwicklung, wird nicht dementsprechend gefördert, wie die Softwarefirmen.

# 7. Schritt: Linux und Geld

* Durch Open Source Software kann nur selten Geld verdient werden.
* Durch Service kann dennoch sehr viel Geld verdient werden.
  So, wie es grosse Firmen wie IBM und HP schon vormachen.
* Linux kann nicht Konkurs gehen, weil es nicht vom Geld anhängig ist.
  Somit ist die Weiterentwicklung so lange gesichert, wie es Personen und Firmen gibt, die für Linux programmieren wollen.

# 8. Schritt: Die Nachteile

* Langsame Entwicklung
* Viele Funktionen nicht verfügbar.
* Administration aufwendiger
* Viele kommerzelle Programme nicht lauffähig.
* Kaum Unterstützung der Hersteller für Dokumentation, Programme und Treiber.
* Gefährdung durch Geschäftsgeheimnisse und Patente.

# 9. Schritt: Die Vorteile

* Offenes System, wenige Abhängigkeiten
* Keine komplizierten Lizenzen
* Hohe Flexibilität
* Hohe Modularität
* Gute technische Dokumentationen
* Einfache Technik
* Einfache Verteilung
* Möglichkeit Anpassungen durchzuführen
* Langfristig günstiger

# 10. Schritt: Die Zukunft

* Die Geschichte hat gezeigt, das sich nur einfache, flexible und offene Systeme durchsetzen.
* Dies gilt zum Beispiel für das Internet.
  Nur durch seine offene Architekur, hat es sich in alle Winkel dieser Erde verbreitet und alle sprachen nur ein einziges Protokoll.
  TCP/IP.
* Auch der PC hatte nur Erfolg, weil er durch Compaq kopiert und so zu einer offenen Architektur wurde.
