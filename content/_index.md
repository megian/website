+++
title = "Megian Website"
description = "Personal website of Megian"
draft = false


# The homepage contents
[extra]
lead = '<b>Megian</b> is a personal website'

# Menu items
[[extra.menu.main]]
name = "Docs"
section = "docs"
url = "/docs"
weight = 10

[[extra.menu.main]]
name = "Blog"
section = "blog"
url = "/blog/"
weight = 20

[[extra.list]]
title = "IT Architecture"
content = 'Get different knowledge about IT Architecture.'

[[extra.list]]
title = "Legacy ⚡️"
content = 'Lot of collected things over decades.'

+++
